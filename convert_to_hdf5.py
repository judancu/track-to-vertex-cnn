# required for loading utilities code
import sys
import os

sys.path.append("%s/../lib" % os.getcwd())

import util
import algos
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from keras.layers import Input
from keras.layers.core import Dense, Dropout
from keras.models import Model
from keras.models import Sequential
from keras import regularizers
from keras.optimizers import Adam

from keras.layers import Dense, Dropout, Flatten, BatchNormalization, Activation
from keras.layers import LSTM, Reshape,Permute, TimeDistributed, Bidirectional
from keras.layers import MaxPooling2D, Conv2D, Conv1D, MaxPooling1D

from keras.models import load_model
from sklearn.preprocessing import StandardScaler

import pickle

#%pylab inline

datasets = {
    "ttbar_9k": "/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-ttbar-pu200-938relval.root",
    "ttbar_100k": "/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-ttbar-pu200-fall17d.root",
    "zmm_100k": "/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-zmm-pu200-fall17d.root",
    "zee_100k": "/home/hep/jd918/project/L1_trigger/data/tmtt-kf4param-ntuple-zee-pu200-fall17d.root",
}

events = util.loadData(datasets['zmm_100k'])
alltracks = pd.concat(events)

def process_events(events):

    max_tracks = 250
    no_features = 10

    X = np.zeros((len(events), max_tracks, no_features))
    y = np.zeros((len(events), 1))
    y_avg = np.zeros((len(events), 1))
    assoc = np.zeros((len(events), max_tracks))

    no_tracks = []

    for i, e in enumerate(events):
        no_tracks.append(e.shape[0])

        tracks = []
        assoc_list = []
        for j, t in e.iterrows():
            params = np.zeros(no_features)
            params[0] = (t['z0'])
            params[1] = (t['pt'])
            params[2] = (t['eta'])
            params[3] = (t['chi2'])
            tracks.append(params)
            assoc_list.append(t['fromPV'])

        # define and populate list of tracks for event
        tr = np.zeros((max_tracks, no_features))
        tr[:e.shape[0]] = np.array(tracks)
        e_assoc = np.zeros((max_tracks))
        e_assoc[:e.shape[0]] = np.array(assoc_list)

        X[i, :] = tr
        y[i] = util.pvz0(e)
        y_avg[i] = util.pvz0_avg(e)
        assoc[i, :] = e_assoc

    print(X.shape)
    print(y.shape)

    print("max tracks = %s" % np.max(no_tracks))

    return X,y, y_avg, no_tracks, assoc

#X, y,_ ,_ ,_ = process_events(events[:2]) # this call tests the operation of the above function with two events

X, y, y_avg, no_tracks, assoc = process_events(events[:2])

from CNNModelWeighted import CNNModelWeighted

network = CNNModelWeighted(
    nbins = 256,
    ntracks = 250,
    nfeatures = 4
)
model = network.makeZ0Model()
model.summary()

optimizer = keras.optimizers.Adam(lr=0.01)
model.compile(loss=network.getLossFunction(), optimizer=optimizer)

model.load_weights(args.weightsFile)

#z_values = model.fit(np.zeros(256, 250, 4)) #argument = # of examples * 250 * 4 (# of params=4; # of tracks = 250)

z_vals = model.fit(X)

import h5py

with h5py.File("zmm-relval-100k-processed.hdf5", "w") as f:
    dset = f.create_dataset("X", X.shape, data = X)
    dset = f.create_dataset("y", y.shape, data = y)
    dset = f.create_dataset("y_avg", y.shape, data = y_avg)
    dset = f.create_dataset("assoc", assoc.shape, data = assoc)
    dset = f.create_dataset("y_e2e", y.shape, data = z_vals)
