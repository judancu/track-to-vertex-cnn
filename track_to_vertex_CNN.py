import sys
import os
#import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import keras
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.externals import joblib
sys.path.append("%s/../lib" % os.getcwd())
import util
import algos
import tensorflow as tf
import scipy.interpolate
import os
import pickle

from keras import backend as K
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import Embedding
from keras.layers import Conv1D, GlobalAveragePooling1D, MaxPooling1D
from keras.layers import Reshape
from keras import optimizers

def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    """
    Freezes the state of a session into a pruned computation graph.

    Creates a new computation graph where variable nodes are replaced by
    constants taking their current value in the session. The new graph will be
    pruned so subgraphs that are not necessary to compute the requested
    outputs are removed.
    @param session The TensorFlow session to be frozen.
    @param keep_var_names A list of variable names that should not be frozen,
                          or None to freeze all the variables in the graph.
    @param output_names Names of the relevant graph outputs.
    @param clear_devices Remove the device directives from the graph for better portability.
    @return The frozen graph definition.
    """
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ""
        frozen_graph = tf.graph_util.convert_variables_to_constants(
            session, input_graph_def, output_names, freeze_var_names)
        return frozen_graph

def matchPerf1(event, vx):
    nPV =  sum(event)
    if nPV == 0:
        return False
    if len(vx) == 0:
        return 0., 0.
    nPV_reco = sum(vx)
    effi = float(nPV_reco) / float(nPV)
    purity = float((nPV_reco)) / len(vx)
    return effi, purity

def CNN(X_CNN, y_CNN, batch_size, title):

    model_CNN = Sequential()

    inputFeatures = keras.layers.Input(shape=(250, 10))
    cnn = keras.layers.Conv1D(32, 1, activation='relu')(inputFeatures)
    cnn = keras.layers.Dropout(0.15)(cnn)
    cnn = keras.layers.Conv1D(32, 1, activation='relu', padding = 'same')(cnn)
    cnn = keras.layers.Dropout(0.15)(cnn)
    predictFraction = keras.layers.Conv1D(1, 1, activation='sigmoid')(cnn)

    model_CNN = keras.models.Model(inputs=[inputFeatures],outputs=[predictFraction])

    def my_loss(y_true, y_pred):
        binary_crossentropy = keras.losses.binary_crossentropy(y_true, y_pred) *tf.sign(inputFeatures[:,:,1])#slice it off the input tensor
        binary_crossentropy = tf.reduce_sum(binary_crossentropy, axis = 1)/tf.reduce_sum(tf.sign(binary_crossentropy), axis = 1)
        return binary_crossentropy

    rmsprop = optimizers.RMSprop(lr=0.01)

    model_CNN.compile(loss=my_loss, optimizer=rmsprop, metrics=['accuracy'])

    y_CNN1 = np.expand_dims(y_CNN, axis=-1)

    testFraction = 0.1
    train_size = int((1.-testFraction) * X_CNN.shape[0])
    Xtrain, Xtest, ytrain, ytest = X_CNN[0:train_size], X_CNN[train_size:], y_CNN1[0:train_size], y_CNN1[train_size:]

    history = model_CNN.fit(Xtrain, ytrain, validation_split=0.33, epochs=20, batch_size=batch_size)
    print(history.history.keys())
    frozen_graph = freeze_session(K.get_session(), output_names=[out.op.name for out in model_CNN.outputs])

    tf.train.write_graph(frozen_graph, "/home/hep/jd918/project/L1_trigger/CNN/", "CNN_model_weights_" + str(title) + ".pb", as_text=False)

    plt.plot(history.history['accuracy'], label="val")
    plt.plot(history.history['val_accuracy'], label="train")
    plt.title('Model accuracy (' + str(title) + ')')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(loc='lower right')
    #plt.savefig('/home/hep/jd918/project/L1_trigger/plots/NN_model_accuracy_Conv.png')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/model_accuracy_'+str(title)+'.png')
    plt.clf()

    plt.plot(history.history['loss'], label="train")
    plt.plot(history.history['val_loss'], label="val")
    plt.title('Model loss (' + str(title) + ')')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(loc='upper right')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/model_loss_'+str(title)+'.png')
    plt.clf()

    probPV = model_CNN.predict(Xtest)

    ytest_flat = ytest.flatten()
    probPV_flat = probPV.flatten()
    Xtest_pt_flat = Xtest[:,:,1].flatten()

    plt.hist(probPV_flat[(ytest_flat==1) & (Xtest_pt_flat>0.0)], range=[0., 1.], bins=50, alpha=0.5, density=True)
    plt.hist(probPV_flat[(ytest_flat==0) & (Xtest_pt_flat>0.0)], range=[0., 1.], bins=50, alpha=0.5, density=True)
    plt.xlabel("predicted weights")
    plt.title('CNN output (' + str(title) + ')')
    plt.legend(['PV', 'PU'], loc='upper right')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/PVvsPU_'+str(title)+'.png')
    plt.clf()

    Efficiency, Purity = [], [] # efficiency, purity
    for cut in np.arange(0., 1., 0.02):
        e, p = matchPerf1(ytest_flat[Xtest_pt_flat>0.0], ytest_flat[Xtest_pt_flat>0.0][probPV_flat[Xtest_pt_flat>0.0] > cut])
        Efficiency.append(e)
        Purity.append(p)

    plt.plot(Efficiency, Purity, '.')
    plt.xlabel("Efficiency")
    plt.ylabel("Matching Purity")
    plt.title('Efficiency vs Purity ROC for association (' + str(title) + ')')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/EffvsPur_'+str(title)+'.png')
    plt.clf()

    AUC = sklearn.metrics.auc(Efficiency, Purity)

    file = open('/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'.txt', "w")
    file.write('AUC of purity vs efficiency: ' + str(AUC))
    file.write("\n")
    auc1 = AUC

    pur = scipy.interpolate.interp1d(Efficiency, Purity)
    file.write('Purity at which Efficiency =95%:  ' + str(pur(0.95)))
    pur1 = float(pur(0.95))
    file.write("\n")

    cut = np.arange(0., 1., 0.02)
    cut_reasonable = scipy.interpolate.interp1d(Efficiency, cut)
    file.write('Reasonable cut at which Efficiency =95%:  ' + str(cut_reasonable(0.95)))
    cut_reasonable1 = float(cut_reasonable(0.95))
    file.write("\n")


    plt.hist2d(probPV_flat[Xtest_pt_flat>0.0], X_test['pt'], range=[[0.0, 1.], [0, 0.4]], bins=50, norm=col.LogNorm());
    plt.xlabel("Network output")#, horizontalalignment='right', x=1.0)
    plt.ylabel("$1/p_t$ (GeV)")#, horizontalalignment='right', y=1.0)
    plt.title('Correlation plot between the network output and $1/p_t$ (' + str(title) + ')')
    plt.colorbar()
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/CNNvspt_'+str(title)+'.png')
    plt.clf()

    plt.hist2d(probPV_flat[Xtest_pt_flat>0.0], X_test['dz'], range=[[0., 1.], [-30, 30]], bins=50, norm=col.LogNorm());
    plt.xlabel("Network output")#, horizontalalignment='right', x=1.0)
    plt.ylabel("dz (cm)")#, horizontalalignment='right', y=1.0)
    plt.title('Correlation plot between the network output and dz (' + str(title) + ')')
    plt.colorbar()
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/CNNvsdz_'+str(title)+'.png')
    plt.clf()

    plt.hist2d(probPV_flat[Xtest_pt_flat>0.0], X_test['chi2'], range=[[0., 1.], [0, 350]], bins=50, norm=col.LogNorm());
    plt.xlabel("Network output")#, horizontalalignment='right', x=1.0)
    plt.ylabel("$\chi^2$")#, horizontalalignment='right', y=1.0)
    plt.title('Correlation plot between the network output and $\chi^2$ (' + str(title) + ')')
    plt.colorbar()
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/CNNvschi2_'+str(title)+'.png')
    plt.clf()

    plt.hist2d(probPV_flat[Xtest_pt_flat>0.0], X_test['eta'], range=[[0., 1.], [-2.5, 2.5]], bins=50, norm=col.LogNorm());
    plt.xlabel("Network output")#, horizontalalignment='right', x=1.0)
    plt.ylabel("$\eta$")#, horizontalalignment='right', y=1.0)
    plt.title('Correlation plot between the network output and $\eta$ (' + str(title) + ')')
    plt.colorbar()
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/CNNvseta_'+str(title)+'.png')
    plt.clf()

    FPR_CNN, TPR_CNN = [], []

    for cut in np.arange(0., 1., 0.001):
        N = len(ytest_flat[Xtest_pt_flat>0.0]) - sum(ytest_flat[Xtest_pt_flat>0.0])
        P = sum(ytest_flat[Xtest_pt_flat>0.0])
        fp = np.sum((probPV_flat[Xtest_pt_flat>0.0] > cut) & (ytest_flat[Xtest_pt_flat>0.0] == 0))
        tp = np.sum((probPV_flat[Xtest_pt_flat>0.0] > cut) & (ytest_flat[Xtest_pt_flat>0.0] == 1))
        if (N != 0):
            FPR_CNN.append(float(fp)/float(N))
            TPR_CNN.append(float(tp)/float(P))

    AUC = sklearn.metrics.auc(TPR_CNN, FPR_CNN)
    file.write('AUC for traditional ROC curve: ' + str(AUC))
    auc2 = AUC
    file.write("\n")

    fpr = scipy.interpolate.interp1d(FPR_CNN, TPR_CNN)
    file.write('TPR at which FPR = 1%:  ' + str(fpr(0.01)))
    fpr1 = float(fpr(0.01))
    file.write("\n")

    plt.semilogy(TPR_CNN, FPR_CNN)
    plt.hlines(1e-2, 0, fpr1, linestyle="dashed")
    plt.vlines(fpr1, 1e-4, 1e-2, linestyle="dashed")
    plt.xlabel("True Positive Rate")
    plt.ylabel("False Positive Rate")
    plt.title('TPR vs FPR ROC for association (' + str(title) + ')')
    plt.savefig('/home/hep/jd918/project/L1_trigger/plots/FPR_vs_TPR_'+str(title)+'.png')
    plt.clf()

    file.close()

    dumplings = [auc1, auc2, fpr1, pur1, history.history['val_loss'][-1], cut_reasonable1]
    fileName = '/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'.pkl'
    fileObject = open(fileName, 'wb')
    #file = open('/home/hep/jd918/project/L1_trigger/textfiles/Output'+str(title)+'.csv', "w")
    pickle.dump(dumplings, fileObject)
    pickle.dump(TPR_CNN, fileObject)
    pickle.dump(FPR_CNN, fileObject)
    pickle.dump(Efficiency, fileObject)
    pickle.dump(Purity, fileObject)
    #file.write('%f, %f, %f',(auc1, auc2, fpr1))
    #file.write("\n")
    #file.write(TPR_CNN)
    #file.write("\n")
    #file.write(FPR_CNN)
    file.close()
